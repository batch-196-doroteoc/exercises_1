///////1////////
//convert to obj, delete last item, add new obj, stringify, log
let courses = `[
		{
			"name" : "Philosophy 101",
			"description" : "Study life!",
			"price" : 2400,
			"isActive" : true
		},
		{
			"name" : "Basic Human Anatomy 101",
			"description" : "Study the human body!",
			"price" : 2300,
			"isActive" : true
		},
		{
			"name" : "History 101",
			"description" : "Study history!",
			"price" : 1500,
			"isActive" : true
		}
	]`;
let parsedCourses = JSON.parse(courses);
let alternativeCourse = {
			name : "Afro-Asian Literature 1001",
			description : "Study prose and poems from the orient!",
			price : 2000,
			isActive : true
};
parsedCourses.splice(2,1, alternativeCourse);
JSON.stringify(parsedCourses);
console.log(parsedCourses);

///////2////////
//log: Martin
let charArr = ["x","/","M","a","r","t","i","n","c","x","v"];
let charrArrSlice = charArr.slice(2,8);
let martin = charrArrSlice.join("");
console.log(martin);

///////3////////
let trainer = {
		name: "Cee",
		age: 10,
		numberOfBadges: 7,
		friends: {
			Hoenn: ["Brendan","May"],
			Kanto: ["Ash","Gary"]
		},
		pokemon: ["Pikachu","Gardevoir","Charizard"],

		introduce: function(){
		console.log(`Hi! I'm ${trainer.name}. I am ${trainer.age} years old! I'm one step away to face the Elite Four with my ${trainer.numberOfBadges} badges!`);
		},

		catch: function(newPokemon){
			if(trainer.pokemon.length>5){
	 		console.log(`A trainer should only have six pokemons to carry!`);
			}else{
		 	trainer.pokemon.push(newPokemon);
	 		console.log(`Gotcha, ` + newPokemon);
	 			}
		},

		release:function(){
			if(trainer.pokemon.length<1){
            console.log(`You have no more pokemons! Catch one first!`);
            }else{
            trainer.pokemon.pop()
            console.log(`Bye bye! See you!`);
            }
		}
};
trainer.introduce();
trainer.catch("Magikarp");
trainer.catch("Psyduck");
trainer.catch("Meowth");
trainer.catch("Slowpoke");
console.log(trainer.pokemon);
trainer.release();
trainer.release();
trainer.release();
trainer.release();
trainer.release();
trainer.release();
trainer.release();
console.log(trainer.pokemon);